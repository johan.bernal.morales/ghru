#!/usr/bin/env nextflow

params.input_dir = false
params.output_dir = false
params.fastq_pattern = false
params.database_dir = false

database_dir=params.database_dir
Channel
    .fromPath(database_dir)
    .ifEmpty { error "Cannot find any reads matching: ${fastqs}" }
    .set {database_ch}

if (params.input_dir) {
  input_dir = params.input_dir - ~/\/$/
  output_dir = params.output_dir - ~/\/$/
  fastq_pattern = params.fastq_pattern
  fastqs = input_dir + '/' + fastq_pattern
  Channel
    .fromFilePairs( fastqs )
    .ifEmpty { error "Cannot find any reads matching: ${fastqs}" }
    .set { raw_fastqs }
}

//Seroba 
process seroba { 
  memory '2 GB'
  conda 'seroba=1.0.1'  
  tag { pair_id }
  publishDir "${output_dir}",
    mode: 'copy',
    saveAs: { file -> "seroba_pneumocat_output/summary.tsv"} 

  input:
  set pair_id, file(file_pair) from raw_fastqs
  file (database_pnemocat) from database_ch

  output:
  set pair_id, file('*.tsv') into summary_ch

  """
  mkdir seroba_pneumocat_output
  seroba runSerotyping ${database_dir} ${file_pait[0]} ${file_pait[1]}

  """
}
//Seroba_summary 
process seroba_sum { 
  memory '2 GB'
  conda 'seroba=1.0.1'  
  publishDir "${output_dir}",
    mode: 'copy',
    saveAs: { file -> "seroba_pneumocat_output/summary.tsv"} 

  input:
  set pair_id, file(csv_files) from summary
   
  output:
  set pair_id, file('*.tsv')

  """
  cd seroba_pneumocat_output
  seroba summary summary_output 
  """
}