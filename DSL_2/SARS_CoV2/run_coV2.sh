nx_data=/path/to/SARS_coV2/directory

nextflow run main.nf \
	--fasta_file ${nx_data}/fasta_file/brazil_ncbi_all_ref.fasta \
	--reference ${nx_data}/references/MN908947.3.fasta \
	--config_dir ${nx_data}/type_variants/config.csv \
    --output_dir . \
	--iqtree \
    -resume
