# __**coV2-nextflow**__

This process generate a phylogeny inference, lineages and variants type assignment <br>
to sars_cov2 based on mapping to reference Wuhan_Hu_1, quality filters (cov and max-ambig), <br>
tree generation (fastree default; iqtree alternative), pangolin prediction software and <br>
typer_variants using nextflow and conda envs to make scalable and easy to use with updated data <br>

**pre-requisites:**

   conda ( pip3 install conda )


**Create manually conda env (optional)**
 
   conda create --name cov --file cov_env.txt <br>


**Software included**

   -minimap2 2.17 <br>
   -datafunk 0.0.8 <br>
   -Fasttree 2.1.10 <br>
   -iqtree 1.6.12 <br>
   -clusterfunk 0.0.3 <br>
   -pangolin 2.3.0 <br>
   -type_variants <br>
   -nextflow 20.10.0 <br>

## _**Run**_

**Edit two first lines in (.sh) file with paths <br>
to nextflow_pipelines directory and to fasta file** <br>

   bash file.sh


## **Access to easy sars_cov2 genomic data analysis is essential to predict risks in LMICs!!**
