nextflow.enable.dsl=2
params.fasta_file = false
params.output_dir = false
params.reference = false
params.iqtree = false
params.config_dir = false

include { RUN_MINIMAP; RUN_SAM_2_FASTA; FILTER_COVG_LENGTH; TREE; ROOT_TREE; LINEAGES_PANGOLIN; TYPE_VARIANTS } from './processes/processes'

workflow {
  if (params.fasta_file && params.output_dir) {
    fastas = Channel
    .from(params.fasta_file)
    .ifEmpty { error "Cannot find any fastas matching: ${params.fasta_file}" }
    
    RUN_MINIMAP(fastas, params.reference)
    RUN_SAM_2_FASTA(RUN_MINIMAP.out.sam_files, params.reference)
    FILTER_COVG_LENGTH(RUN_SAM_2_FASTA.out)
    TREE(FILTER_COVG_LENGTH.out)
    ROOT_TREE(TREE.out)
    LINEAGES_PANGOLIN(fastas)
    TYPE_VARIANTS(RUN_SAM_2_FASTA.out, params.config_dir, params.reference)
  }
}
