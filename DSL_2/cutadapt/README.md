# __**cutadapt-nextflow**__

This process put together cutadapt and nextflow for <br> 
scalable cleaning internal adapters contamination of <br>
raw reads <br>

**pre-requisites:**

   conda ( pip3 install conda )

**Create manually conda env (optional)**
 
   conda create --name cutadaptenv --file <br> 
   env_conda_fastqc_cutadapt.txt <br>

_**Run**_

**Edit two first lines in (.sh) file with paths <br>
to nextflow_pipelines directory and data into fastqs <br> 
directory** <br>

   sh file.sh

Have a look on your fastqs pattern <br>
 (ej.'*{_,R}{1,2}.f*q.gz') <br>

prunning -> adapter_cleaning <br>

## **Now, you recovered your unvaluable files!!**

**Authors:**
[Angela Garcia](https://gitlab.com/angela_s_garcia_v) <agarciav@agrosavia.co> <br>
[Felipe Delgadillo Barrera](https://gitlab.com/felipe_delgadillo_barrera) <f.delgadillo2628@uniandes.edu.co> <br>
[Oscar Beltran](https://gitlab.com/ogbeltran)<obeltran@agrosavia.co> <br>
[Johan Fabian Bernal](https://gitlab.com/johan.bernal.morales) <jfbernal@agrosavia.co> <br>
